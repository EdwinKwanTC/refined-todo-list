import "./App.css";
import React from "react";

// components
import HomePage from "./components/HomePage";
import Footer from "./components/Footer";

function App() {
  return (
    <>
      <HomePage />
      <Footer />
    </>
  );
}

export default App;
